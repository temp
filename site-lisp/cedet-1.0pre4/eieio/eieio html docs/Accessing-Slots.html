<html lang="en">
<head>
<title>Accessing Slots - Enhanced Implementation of Emacs Interpreted Objects</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name="description" content="Enhanced Implementation of Emacs Interpreted Objects">
<meta name="generator" content="makeinfo 4.8">
<link title="Top" rel="start" href="index.html#Top">
<link rel="prev" href="Making-New-Objects.html#Making-New-Objects" title="Making New Objects">
<link rel="next" href="Writing-Methods.html#Writing-Methods" title="Writing Methods">
<link href="http://www.gnu.org/software/texinfo/" rel="generator-home" title="Texinfo Homepage">
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css"><!--
  pre.display { font-family:inherit }
  pre.format  { font-family:inherit }
  pre.smalldisplay { font-family:inherit; font-size:smaller }
  pre.smallformat  { font-family:inherit; font-size:smaller }
  pre.smallexample { font-size:smaller }
  pre.smalllisp    { font-size:smaller }
  span.sc    { font-variant:small-caps }
  span.roman { font-family:serif; font-weight:normal; } 
  span.sansserif { font-family:sans-serif; font-weight:normal; } 
--></style>
</head>
<body>
<div class="node">
<p>
<a name="Accessing-Slots"></a>
Next:&nbsp;<a rel="next" accesskey="n" href="Writing-Methods.html#Writing-Methods">Writing Methods</a>,
Previous:&nbsp;<a rel="previous" accesskey="p" href="Making-New-Objects.html#Making-New-Objects">Making New Objects</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="index.html#Top">Top</a>
<hr>
</div>

<!-- node-name,  next,  previous,  up -->
<h2 class="chapter">6 Accessing Slots</h2>

<p>There are several ways to access slot values in an object.  The naming
convention and argument order is similar to that found in Emacs Lisp for
referencing vectors.  The basics for referencing, setting, and calling
methods are all accounted for.

<div class="defun">
&mdash; Function: <b>oset</b><var> object slot value<a name="index-oset-14"></a></var><br>
<blockquote>
<p>This sets the value behind <var>slot</var> to <var>value</var> in <var>object</var>. 
<code>oset</code> returns <var>value</var>. 
</p></blockquote></div>

<div class="defun">
&mdash; Function: <b>oset-default</b><var> class slot value<a name="index-oset_002ddefault-15"></a></var><br>
<blockquote>
<p>This sets the slot <var>slot</var> in <var>class</var> which is initialized with
the <code>:initform</code> tag to <var>value</var>.  This will allow a user to set
both public and private defaults after the class has been constructed. 
This function is intrusive, and is offered as a way to allow users to
configure the default behavior of packages built with classes the same
way <code>setq-default</code> is used for buffer-local variables.

        <p>For example, if a user wanted all <code>data-objects</code> (see <a href="Building-Classes.html#Building-Classes">Building Classes</a>) to inform a special object of his own devising when they
changed, this can be arranged by simply executing this bit of code:

     <pre class="example">          (oset-default data-object reference (list my-special-object))
     </pre>
        </blockquote></div>

<div class="defun">
&mdash; Function: <b>oref</b><var> object slot<a name="index-oref-16"></a></var><br>
<blockquote>
<p>This recalls the value in slot <var>slot</var> in <var>object</var> and returns
it.  If <var>object</var> is a class, and <var>slot</var> is a class allocated
slot, then oref will return that value.  If <var>object</var> is a class, and
<var>slot</var> is not class allocated, a signal will be thrown. 
</p></blockquote></div>

<div class="defun">
&mdash; Function: <b>oref-default</b><var> object slot<a name="index-oref_002ddefault-17"></a></var><br>
<blockquote><p>This gets the default value in <var>object</var>'s class definition for
<code>slot</code>.  This can be different from the value returned by
<code>oref</code>.  <var>object</var> can also be a class symbol or an instantiated
object. 
</p></blockquote></div>

   <p>These next accessors are defined by CLOS to reference or modify slot
values, and use the previously mentioned set/ref routines.

<div class="defun">
&mdash; Function: <b>slot-value</b><var> object slot<a name="index-slot_002dvalue-18"></a></var><br>
<blockquote><p>This function retrieves the value of <var>slot</var> from <var>object</var>. 
Unlike <code>oref</code>, the symbol for <var>slot</var> must be quoted in. 
</p></blockquote></div>

<div class="defun">
&mdash; Function: <b>set-slot-value</b><var> object slot value<a name="index-set_002dslot_002dvalue-19"></a></var><br>
<blockquote><p>This is not a CLOS function, but is meant to mirror <code>slot-value</code> if
you don't want to use the cl package's <code>setf</code> function.  This
function sets the value of <var>slot</var> from <var>object</var>.  Unlike
<code>oset</code>, the symbol for <var>slot</var> must be quoted in. 
</p></blockquote></div>

<div class="defun">
&mdash; Function: <b>slot-makeunbound</b><var> object slot<a name="index-slot_002dmakeunbound-20"></a></var><br>
<blockquote><p>This function unbinds <var>slot</var> in <var>object</var>.  Referencing an
unbound slot can throw an error. 
</p></blockquote></div>

<div class="defun">
&mdash; Function: <b>object-add-to-list</b><var> object slot &amp;optional append<a name="index-object_002dadd_002dto_002dlist-21"></a></var><br>
<blockquote><p>In <var>OBJECT</var>'s <var>SLOT</var>, add <var>ITEM</var> to the pre-existing list of elements. 
Optional argument <var>APPEND</var> indicates we need to append to the list. 
If <var>ITEM</var> already exists in the list in <var>SLOT</var>, then it is not added. 
Comparison is done with <dfn>equal</dfn> through the <dfn>member</dfn> function call. 
If <var>SLOT</var> is unbound, bind it to the list containing <var>ITEM</var>. 
</p></blockquote></div>

<div class="defun">
&mdash; Function: <b>with-slots</b><var> entries object forms<a name="index-with_002dslots-22"></a></var><br>
<blockquote><p>Bind <var>entries</var> lexically to the specified slot values in
<var>object</var>, and execute <var>forms</var>.  In CLOS, it would be possible to
set values in OBJECT by using <code>setf</code> to assign to these values, but
in Emacs, you may only read the values, or set the local variable to a
new value.

     <pre class="example">          (defclass myclass () (x :initarg 1))
          (setq mc (make-instance 'myclass))
          (with-slots (x) mc x)                      =&gt; 1
          (with-slots ((something x)) mc something)  =&gt; 1
     </pre>
        </blockquote></div>

   </body></html>


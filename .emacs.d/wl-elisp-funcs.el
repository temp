;wl_elisp.el
(defun wombat () "Load the wombat color theme."
        (interactive)
        (if (file-exists-p "~/.emacs.d/color-theme/themes/color-theme-wombat.el")
        (load-file "~/.emacs.d/color-theme/themes/color-theme-wombat.el")))

(defun reload () "Reloads .emacs interactively"
        (interactive)
    (if (file-exists-p "~/.emacs")
    (load-file "~/.emacs")))

(defun count-words-buffer ()
  "Count the number of words in the current buffer;
print a message in the minibuffer with the result."
  (interactive)
  (save-excursion
    (let ((count 0))
      (goto-char (point-min))
      (while (< (point) (point-max))
        (forward-word 1)
        (setq count (1+ count)))
      (message "buffer contains %d words." count))))

; NOTE:  I attempted to emulate a solution I found here:
;http://stackoverflow.com/questions/1037545/how-to-replace-forwardslashes-with-backslashes-in-a-string-in-lisp-for-emacs
; but failed.  Wound up using this which I got from running replace-regexp on a region and hitting C-x ESC ESC and pasted this into my func:
;       (replace-regexp "\\\\" "/" nil (if (and transient-mark-mode mark-active) (region-beginning)) (if (and transient-mark-mode mark-active) (region-end)))
; Currently examining this tutorial so I can figure out why the stackoverflow solution failed.
;http://xahlee.org/emacs/lisp_regex_replace_func.html
(defun replace-backslashes () "Replace the backslashes with slashes."
        (interactive)
        (save-excursion)
        (replace-regexp "\\\\" "/" nil
        (if (and transient-mark-mode mark-active) (region-beginning))
                (if (and transient-mark-mode mark-active) (region-end))))

;;The following came from here:
;http://lispy.wordpress.com/category/regular-expressions/
;It's a good example of how you can set up text processing routines
;Using defmacro instead of just a defun lets you defines the key binding at the same time that the replacement function is defined.  This also has it's drawbacks, lispy gives this as his rationale:
;I went with a macro because once I get my regular expression from re-builder, I wanted to be able to write the code for everything as quickly as possible. With “defreplacer”, all I needed was four arguments and I was good to go.
(defmacro defreplacer (name description search-for replace-with chord)
  `(progn
     (defun ,name (n)
       ,description
       (interactive "p")
       (query-replace-regexp ,search-for
                             ,replace-with
                             nil
                             (if (and transient-mark-mode mark-active)
                                 (region-beginning))
                             (if (and transient-mark-mode mark-active)
                                 (region-end))))
     (global-set-key (kbd ,chord) ',name)))

(defreplacer pull-text-from-semi-colons
 "Remove text from between two semi-colon signs."
  "[ ]*;\\([a-z]*\\);[ ]*" ; use double back slash to 'escape' in quotes
  "\\1"
  "C-c ;")


(defun remove-blank-lines (n)
"Removes blank lines from a buffer"
(interactive "p") ; n = value passed by the C-u “universal argument”
(query-replace-regexp "

+" "
" nil
(if (and transient-mark-mode mark-active) (region-beginning))
(if (and transient-mark-mode mark-active) (region-end))))